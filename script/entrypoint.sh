#!/bin/sh
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py test fibo.tests -v 2
python3 manage.py runserver --verbosity 3 0.0.0.0:8000
exec "$@"