from rest_framework.views import APIView
from django.http import JsonResponse
from rest_framework.decorators import api_view, schema
import logging

from .tools.fibonacci import get_indexed_range

logger = logging.getLogger(__name__)


class Fibonacci(APIView):
    @api_view(['GET'])
    @schema(None)
    def get(request):
        from_index = to_index = None
        try:
            from_index = int(request.GET.get('from'))
            to_index = int(request.GET.get('to'))
        except ValueError as ex:
            logger.error('The requested range could not be calculated', exc_info=True)
            return JsonResponse({'error': ''.join(ex.args)}, status=400, safe=False)
        except TypeError as ex:
            if from_index is None or to_index is None:
                msg = 'From and To parameters should be provided as integers'
                logger.error(msg)
                return JsonResponse({'error': msg}, status=400, safe=False)
        try:
            result = get_indexed_range(from_index, to_index)
        except IndexError as ex:
            logger.error('The requested range could not be calculated', exc_info=True)
            return JsonResponse({'error': ''.join(ex.args)}, status=400, safe=False)

        return JsonResponse(result, safe=False)
