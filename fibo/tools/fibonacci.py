import logging
import redis
from django.conf import settings

logger = logging.getLogger(__name__)
_hash_name = "fibohash"

r = redis.StrictRedis(host=settings.REDIS_HOST,
                      port=settings.REDIS_PORT,
                      db=settings.REDIS_DB,
                      charset=settings.REDIS_CHARSET,
                      decode_responses=settings.REDIS_DECODE_RESPONSE
                      )


def calculate_from_parents(x, y):
    """
    A lighter calculation than calculate_from_index. This can be used when both parents are known.
    :param x: Value of Fib[n-2]
    :param y: Value of Fib[n-1]
    :return: The Fibonacci number that results from the two given parents (Fib[n])
    """
    return x + y


def calculate_from_index(index):
    """
    Calculates the Fibonacci number at the given index.
    As this is an expensive operation, the result is also stored in cache.
    :param index: The index of the Fibonacci number to return
    :return: The Fibonacci number at the given index
    """
    if index == 0:
        result = index
    elif 2 >= index:
        result = 1
    else:
        t = 1 << index.bit_length()  # Get the closest, higher, base 2 number to index
        t = int(t/4)
        fnext, fcur, fprev = 1, 1, 0
        while t > 0:
            B = fcur * fcur
            fcur *= fnext + fprev
            fnext = fnext * fnext + B
            fprev = fprev * fprev + B
            if index & t:
                fprev = fcur
                fcur = fnext
                fnext = fcur + fprev
            t = int(t/2)
        result = fcur
    return result


def get_indexed_range(index_from, index_to):
    """
    Determines the Fibonacci numbers in the given range.
    Stores the results in cache for later use.
    :param index_from: The index (integer) to start from
    :param index_to: The index (integer) to stop at
    :return: An ordered list of Fibonacci numbers in the given index range
    """
    ex_index_msg = '{} must be equal to, or higher than, {}'

    if index_from < 0:
        raise IndexError(ex_index_msg.format('index_from', '0'))
    if settings.MAX_FIBONACCI_INDEX > -1 and index_to > settings.MAX_FIBONACCI_INDEX:
        raise IndexError('index_to must be lower than {}'.format(settings.MAX_FIBONACCI_INDEX))
    if index_to < index_from:
        raise IndexError(ex_index_msg.format('index_to', 'index_from'))

    index_to += 1  # Increment to include it's outcome in the range
    from_cache = r.hmget(_hash_name, (range(index_from, index_to)))  # Get all values in the given range from cache

    if None not in from_cache:   # There are no empty positions, so the entire list was already cached
        result = from_cache
    else:  # There are empty values which need to be calculated
        cache_dict = dict(zip((range(index_from, index_to)), from_cache))  # Zip values list to dict to work with indexes
        for key, value in cache_dict.items():
            if value is None:
                x = cache_dict.get(key - 2)
                y = cache_dict.get(key - 1)
                if x and y:  # Check if the parents are known
                    value = calculate_from_parents(int(x), int(y))
                else:
                    value = calculate_from_index(key)
                r.hset(_hash_name, key, value)  # Store the calculated value in cache
                cache_dict[key] = value
        result = cache_dict.values()
    return list(map(int, result))
