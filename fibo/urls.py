from django.conf.urls import url
from django.contrib import admin

from .views import Fibonacci

urlpatterns = [
    url(r'fibonachi/$', Fibonacci.get, name='fibonachi')
]
