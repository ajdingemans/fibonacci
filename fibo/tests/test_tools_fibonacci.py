from django.test import TestCase

from fibo.tools.fibonacci import calculate_from_index, calculate_from_parents, get_indexed_range

import logging
logger = logging.getLogger(__name__)


class FibonacciTestCase(TestCase):
    """ Test suite for the Fibonacci helper functions. """

    def test_get_small_fib(self):
        """ Getting small Fibonacci numbers """
        self.assertEquals(calculate_from_index(0), 0)
        self.assertEquals(calculate_from_index(1), 1)
        self.assertEquals(calculate_from_index(2), 1)
        self.assertEquals(calculate_from_index(3), 2)
        self.assertEquals(calculate_from_index(4), 3)
        self.assertEquals(calculate_from_index(5), 5)
        self.assertEquals(calculate_from_index(6), 8)
        self.assertEquals(calculate_from_index(10), 55)
        self.assertEquals(calculate_from_index(11), 89)

    def test_get_large_fib(self):
        """ Getting large Fibonacci numbers """
        self.assertEquals(calculate_from_index(1000), 43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875)

    def test_get_from_parents(self):
        """ Getting the Fibonacci number based on the two parents. """
        self.assertEquals(calculate_from_parents(5, 8), 13)

    def test_get_indexed_range(self):
        """ Getting a range of Fibonacci numbers """
        test_range = [0, 1, 1, 2, 3, 5]
        test_range2 = [3, 5, 8, 13]
        self.assertEquals(get_indexed_range(0, 5), test_range)
        self.assertEquals(get_indexed_range(4, 7), test_range2)

    def test_infinite_max_fib(self):
        """ Exceeding the maximum Fibonacci number set by MAX_FIBONACCI_INDEX"""
        test_range = [3, 5, 8, 13]
        with self.settings(MAX_FIBONACCI_INDEX=-1):
            self.assertEquals(get_indexed_range(4, 7), test_range)

    def test_ex_get_indexed_range_low_bounds(self):
        """ Get an exception on a range starting below 0 """
        self.assertRaises(IndexError, get_indexed_range, -1, 3)

    def test_ex_get_indexed_range_negative_range(self):
        """ Get an exception on a range starting higher than it's ending value """
        self.assertRaises(IndexError, get_indexed_range, 10, 4)

    def test_ex_get_indexed_range_non_numeric(self):
        """ Get an exception on a range containing non numeric values """
        self.assertRaises(TypeError, get_indexed_range, 'a', 9)
        self.assertRaises(TypeError, get_indexed_range, '0', 9)

    def test_ex_exceed_max_fib(self):
        """ Get an exception on exceeding the maximum Fibonacci number set by MAX_FIBONACCI_INDEX """
        with self.settings(MAX_FIBONACCI_INDEX=12):
            self.assertRaises(IndexError, get_indexed_range, 10, 13)


