from django.test import Client, TestCase
from django.urls import reverse

client = Client()


class FibonacciTestCase(TestCase):
    def test_without_parameters(self):
        """
        If no parameters are supplied, an error is shown
        :return:
        """
        response = self.client.get(reverse('fibonachi'))
        self.assertEqual(response.status_code, 400)

    def test_invalid_page(self):
        """
        If an invalid page is supplied, an appropriate result is displayed
        """
        response = self.client.get('fibo', {'from': 0, 'to': 3})
        self.assertEqual(response.status_code, 404)

    def test_negative_from_input(self):
        """
        If a negative index is supplied for From and/or To, an appropriate result is displayed
        """
        response = self.client.get(reverse('fibonachi'), {'from': -1, 'to': 3})
        self.assertEqual(response.status_code, 400)

    def test_negative_to_input(self):
        """
        If a negative index is supplied for From and/or To, an appropriate result is displayed
        """
        response = self.client.get(reverse('fibonachi'), {'from': 0, 'to': -3})
        self.assertEqual(response.status_code, 400)

    def test_non_numeric_from_input(self):
        """
        If a negative index is supplied for From and/or To, an appropriate result is displayed
        """
        response = self.client.get(reverse('fibonachi'), {'from': 'a', 'to': 10})
        self.assertEqual(response.status_code, 400)

    def test_non_numeric_to_input(self):
        """
        If a negative index is supplied for From and/or To, an appropriate result is displayed
        """
        response = self.client.get(reverse('fibonachi'), {'from': 0, 'to': 'a'})
        self.assertEqual(response.status_code, 400)

    def test_post_request(self):
        """
        POST requests should not be handled and result in a Method not allowed error (405)
        """
        response = self.client.post(reverse('fibonachi'), {'from': 0, 'to': 10})
        self.assertEqual(response.status_code, 405)

    def test_correct_input(self):
        """
        If a valid from and to parameter are supplied, an appropriate result is displayed
        """
        response = self.client.get(reverse('fibonachi'), {'from': 0, 'to': 3})
        self.assertEqual(response.status_code, 200)
